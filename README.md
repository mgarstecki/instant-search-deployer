# Instant search app sample deployer

This repo provides a way to deploy the Algolia [instant search demo](https://github.com/algolia/instant-search-demo) with Ansible.
It uses Vagrant to provide a local VM to test the deployment.


## Prerequisites

To test the deployer, you will need to install locally:
* **Vagrant**. See the [Vagrant docs](https://www.vagrantup.com/intro/getting-started/index.html) for install instructions.
  The deployer has been tested with Vagrant 2.1.2 and its default VirtualBox provider on Linux.
* **Ansible**. The deployer has been tested with Ansible 2.7.4. Since OS packages are rarely up to date,
  using `virtualenv` to get the latest version is generally a good idea.
  To install and use Ansible with `virtualenv`:
  ```bash
  # Depending on your OS, you might need to install pip itself first.
  $ sudo pip install virtualenv
  $ virtualenv ./venv
  $ source venv/bin/activate
  (venv) $ pip install ansible
  ansible --version
  ansible 2.7.4
    config file = None
    [...]
  ```

## How to deploy

Clone this repo, and make sure Ansible and Vagrant are installed and available (see prerequisites).

`cd` into the cloned repo, then run `vagrant up`  to launch and configure the demo VM with Ansible. This will:
* Launch an Ubuntu 18.04 VM with Vagrant and Virtualbox.
* Run the `ansible/deploy.yml` playbook inside the VM. The playbook itself will:
  * Install NodeJS.
  * Deploy and run the [instant search demo](https://github.com/algolia/instant-search-demo) on port 3000.

Vagrant is configured to forward host port 3000 to the same port on the VM. If port 3000 is already used on your host,
deployment will fail.
You can choose another port by modifying the `host: 3000` directive in `Vagrantfile` and launching `vagrant up` again.

Once deployment has finished succesfully, you can access the demo application at http://127.0.0.1:3000/.


## Running Ansible again in the VM, modifying deployment parameters

You can launch Ansible again in the VM using `./ansible-playbook-wrapper.sh`. You can pass `ansible-playbook` params to the script and they will be passed to Ansible:
```bash
$ ./ansible-playbook-wrapper.sh -v
$ ./ansible-playbook-wrapper.sh -e instant_search_revision=renovate/npm-6.x
$ [...]
```

The following Ansible variables can be provided to the script to modify the deployment parameters:
* `-e instant_search_revision=GIT_REVISION`: deploy the GIT_REVISION version of the [demo](https://github.com/algolia/instant-search-demo) repo.
* `-e instant_search_force_new_deploy=true`: to force a redeployment even if the Git revision hasn't changed.
* `-e instant_search_port=4000`: to change the listen port for the app.
  * *Note:* If you change the listen port, make sure to change the `guest: 3000` part in the Vagrantfile too, and to run `vagrant halt` then `vagrant up` to keep access to the app.

You can see a list of available variables in `ansible/roles/instant-search/defaults/main.yml`.
