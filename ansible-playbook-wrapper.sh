#!/usr/bin/env bash

set -o errexit

ansible-playbook -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory ansible/deploy.yml $@
